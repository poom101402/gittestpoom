﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class changeScene : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void Change(string nameScene){
		SceneManager.LoadScene(nameScene);
	}

	public void ChangeTo360Room(string id){
		if(id.Equals("A1")){
			Room360._roomIdFolder = id;
			LoadZipRoom360(id);
		}
		else if(id.Equals("C11")){
			Room360._roomIdFolder = id;
			LoadZipRoom360(id);
		}
		SceneManager.LoadScene("showRoom360");
	}

	void LoadZipRoom360(string idZipFolder){
		//load zip จากดาต้าเบสที่ฟังก์ชันนี้ เพื่อสร้าง folder ใหม่ชื่อตาม set ภาพเก็บไว้ในโฟลเดอร์ ZipFile แล้วเก็บภาพทั้งหมดไว้ 
		print(Room360._roomIdFolder);
	}
}
